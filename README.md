# itay-game-loop
A game loop written in typescript.

## Install
`npm install --save itay-game-loop`

## Usage
```ts
import { MainLoop, FpsMonitor } from "itay-game-loop";

export class Game {
    private fpsMonitor = new FpsMonitor();
    private mainLoop = new MainLoop();
    private fpsDisplay: HTMLElement | null;

    constructor() {
        this.fpsDisplay = document.getElementById('fpsDisplay');

        this.mainLoop.timing.maxTicksPerSecond = 65;
        this.mainLoop.setTick(delta => this.update(delta));
    }

    private update(deltaMillisec: number) {
        this.fpsMonitor.countFrame(deltaMillisec);

        if (this.fpsDisplay) {
            this.fpsDisplay.textContent = Math.round(this.fpsMonitor.fps) + ' FPS';
        }
    }
}
```
