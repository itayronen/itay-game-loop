import { TimingController } from "./TimingController";

type TickFunction = (deltaMillisec: number) => void;

declare var global: Window;
declare var self: {};
var _self: Window = (typeof self !== "undefined" && self as any) ||
	(typeof global !== "undefined" && global) || undefined!;

export class MainLoop {
	public timing: TimingController = new TimingController();
	public requestFrame: (callback: FrameRequestCallback, waitMillisec: number) => number;
	public cancelFrame: (handle: number) => void;
	public vebrose = false;

	private frameId: number = 0;
	private lastTickTime: number = 0;
	private _isRunning = false;
	private tickFunc: TickFunction = function () { };

	constructor() {
		this.requestFrame = _self.requestAnimationFrame ?
			_self.requestAnimationFrame.bind(self) :
			function (this: MainLoop, callback: FrameRequestCallback, waitMillisec: number) {
				return _self.setTimeout(() => { callback(Date.now()) }, waitMillisec);
			};

		this.cancelFrame = _self.requestAnimationFrame ?
			_self.cancelAnimationFrame.bind(self) :
			function (this: MainLoop, handle: number) {
				_self.clearTimeout(handle);
			};
	}

	public get isRunning(): boolean {
		return this._isRunning;
	}

	public start(): void {
		if (this._isRunning) {
			return;
		}

		this._isRunning = true;

		this.lastTickTime = 0;

		this.frameId = this.requestFrame(time => {
			this.lastTickTime = time - this.timing.minMillisecondsPerTick;
			this.mainLoop(time);
		}, 0);
	}

	public stop() {
		if (!this._isRunning) {
			return;
		}

		this._isRunning = false;

		this.cancelFrame(this.frameId);
	}

	public setTick(tickFunc: ((deltaMillisec: number) => void) | null): void {
		this.tickFunc = tickFunc || function () { };
	}

	private mainLoop: (time: number) => void = time => {
		let deltaMillisec = time - this.lastTickTime;

		if (deltaMillisec < this.timing.minMillisecondsPerTick) {
			this.frameId = this.requestFrame(this.mainLoop, this.timing.minMillisecondsPerTick - deltaMillisec);

			if (this.vebrose) {
				console.log("skip - min: " + this.timing.minMillisecondsPerTick + " delta:" + deltaMillisec);
			}

			return;
		}

		this.frameId = this.requestFrame(this.mainLoop, this.timing.minMillisecondsPerTick);

		if (deltaMillisec > this.timing.maxMillisecondsPerTick) {
			// TODO: Trigger event.

			deltaMillisec = this.timing.maxMillisecondsPerTick;
		}

		this.lastTickTime = time;
		
		this.tickFunc(deltaMillisec);
	}
}
