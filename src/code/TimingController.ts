export class TimingController {
    private _minMillisecondsPerTick: number = 0;
    private _maxMillisecondsPerTick: number = Number.MAX_VALUE;

    public get minMillisecondsPerTick(): number {
        return this._minMillisecondsPerTick;
    }

    public set minMillisecondsPerTick(value: number) {
        if (value < 0) {
            this._minMillisecondsPerTick = 0;
        }
        else {
            this._minMillisecondsPerTick = value;
        }
    }

    public get maxMillisecondsPerTick(): number {
        return this._maxMillisecondsPerTick;
    }

    public set maxMillisecondsPerTick(value: number) {
        if (value <= 0) {
            this._maxMillisecondsPerTick = Number.MAX_VALUE;
        }
        else {
            this._maxMillisecondsPerTick = value;
        }
    }

    public get minTicksPerSecond(): number {
        return 1000 / this._maxMillisecondsPerTick;
    }

    public set minTicksPerSecond(value: number) {
        if (value <= 0) {
            this.maxMillisecondsPerTick = 0;
        }
        else {
            this.maxMillisecondsPerTick = 1000 / value;
        }
    }

    public get maxTicksPerSecond(): number {
        return 1000 / this._minMillisecondsPerTick;
    }

    public set maxTicksPerSecond(value: number) {
        if (value <= 0) {
            this.minMillisecondsPerTick = 0;
        }
        else {
            this.minMillisecondsPerTick = 1000 / value;
        }
    }
}