export class FpsMonitor {
    public fps = 60;

    private sampleIntervalMillisec = 500;

    private timestamp = 0;
    private framesThisInterval = 0;
    private sampleStartTimestamp = 0;

    public countFrame(deltaMillisec: number): void {
        this.timestamp += deltaMillisec;

        if (this.hasSampleTimeEnded()) {
            this.sampleStartTimestamp = this.timestamp;
            this.fps = this.framesThisInterval * (1000 / this.sampleIntervalMillisec);
            this.framesThisInterval = 0;
        }

        this.framesThisInterval++;
    }

    private hasSampleTimeEnded(): boolean {
        return this.timestamp > this.sampleStartTimestamp + this.sampleIntervalMillisec;
    }
}