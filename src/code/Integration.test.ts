import { TestParams, TestSuite } from "just-test-api";
import { expect } from "chai";
import { MainLoop } from "./MainLoop";

export default function (suite: TestSuite): void {
	suite.describe("Integration", suite => {
		suite.test("The loop is ticking.", async test => {
			test.arrange();
			let loop = new MainLoop();
			let hasTicked = false;
			loop.setTick(() => { hasTicked = true; });

			test.act();
			loop.start();
			await new Promise(resolve => setTimeout(resolve, 20));

			test.assert();
			expect(hasTicked).to.be.true;
		});

		suite.test("Cancel.", async test => {
			test.arrange();
			let loop = new MainLoop();
			loop.timing.minMillisecondsPerTick = 1;
			let tickCount = 0;

			loop.setTick(() => {
				tickCount++;

				if (tickCount === 2) {
					loop.stop();
				}
			});

			test.act();
			loop.start();
			await new Promise(resolve => setTimeout(resolve, 10));

			test.assert();
			expect(tickCount).to.equal(2);
		});

		suite.describe("Timming.", async suite => {
			suite.test("Min milliseconds per tick. This might fall if cpu is busy.", async test => {
				test.arrange();
				let loop = new MainLoop();
				loop.timing.minMillisecondsPerTick = 10;

				let tickCount = 0;
				loop.setTick(() => { tickCount++; });

				test.act();
				loop.start();

				let testMillisec = 150;
				await new Promise(resolve => setTimeout(resolve, testMillisec));

				let expected = testMillisec / loop.timing.minMillisecondsPerTick;

				test.assert();
				expect(expected - tickCount).to.be.lessThan(5);
			});
		});
	});
}